package controller;

import controller.file.data.input.DataProgramUtils;
import model.entity.cell.standard.prefabCell.CellCreatorImpl;
import model.genome.decryptor.GeneDecryptor;
import model.genome.decryptor.GeneDecryptorImpl;
import model.genome.factory.GenesFactoryImpl;
import model.genome.factory.GenesManagerImpl;
import model.initialization.WorldInitialization;
import model.properties.cells.CellData;
import model.properties.cells.CellDataBuilderImpl;
import model.properties.defaultdata.CellsDefaultUtils;
import model.properties.genes.GenesData;
import model.properties.genes.GenesDataBuilderImpl;
import model.world.World;
import model.world.WorldImp;

/**
 * 
 * Initialize and launch simulation.
 *
 */
public class SimulationInitializator {

    /**
     * build and lunch simulation.
     */
    public SimulationInitializator() {
        //create world
        final World world = new WorldImp(DataProgramUtils.getWorldWidth(), DataProgramUtils.getWorldHeight());
        //create the data needed for creating genes by GenesFactory
        final GenesData genesData = new GenesDataBuilderImpl(world)
                                .setSunEnergy(DataProgramUtils.getMaxLight())
                                .setSunPenetration(DataProgramUtils.getSunPenetration())
                                .setMineralsDepth(DataProgramUtils.getMineralDepth())
                                .build();

        //create a gene decryptor needed for creating CellData. 
        final GeneDecryptor decryptor = new GeneDecryptorImpl(
                CellsDefaultUtils.NUMBER_OF_GENES.getDafaultValue(),
                new GenesManagerImpl(new GenesFactoryImpl(genesData)));

        //create cell data needed for creating a first cell.
        final CellData cellData = new CellDataBuilderImpl(decryptor)
                                .setMaxAge(DataProgramUtils.getMaxAge())
                                .setGenomeSize(DataProgramUtils.getSizeGenoma())
                                .setMaxEnergy(DataProgramUtils.getMaxEnergy())
                                .build();

        //initialize world with the first cells.
        new WorldInitialization(world, new CellCreatorImpl(cellData)).initialize(DataProgramUtils.getModProg());
        //start simulation.
        new Thread(new Start(world)).start();
    }

}
