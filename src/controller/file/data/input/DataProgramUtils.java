package controller.file.data.input;

import controller.StartingMode;
import model.color.filter.Filters;
import model.properties.defaultdata.CellsDefaultUtils;
import model.properties.defaultdata.GenesDefaultUtils;
import model.properties.defaultdata.ViewDefaultUtils;
import model.properties.defaultdata.WorldDefaultUtils;

/**
 * Utility class containing all the values necessary for the operation of the program entered by the user or taken for default.
 */
public final class DataProgramUtils {
    private static int maxEnergy = CellsDefaultUtils.MAX_CELL_ENERGY.getDafaultValue();
    private static int maxLight = GenesDefaultUtils.SUN_ENERGY.getDafaultValue();
    private static int sizeGenoma = CellsDefaultUtils.GENOME_SIZE.getDafaultValue();
    private static int maxAge = CellsDefaultUtils.MAX_AGE.getDafaultValue();
    private static int upDateView = ViewDefaultUtils.UPDATE_VIEW.getDafaultValue();
    private static int worldHeight = WorldDefaultUtils.WORLD_HEIGHT.getDafaultValue();
    private static int worldWidth = WorldDefaultUtils.WORLD_WIDTH.getDafaultValue();
    private static Filters colorFilter = Filters.AGE;
    private static StartingMode modProg = StartingMode.SINGLE_PHOTOSYNTHESIS_CELL;
    private static float hueColor = ViewDefaultUtils.COLOR_HSB_RANGE.getDafaultValue();
    private static float sunPenetration = GenesDefaultUtils.SUN_PENETRATION.getDafaultValue();
    private static float mineralDepth = GenesDefaultUtils.MINERALS_DEPTH.getDafaultValue();
    private static FileGetSetting file = new FileGetSettingImpl();

    private DataProgramUtils() {
    }

    /**
     * Method that returns the starting mode in which the program will take place either with random gene or with gene 
     * given by photosynthesis set by the user.
     * @return starting mode
     */
    public static StartingMode getModProg() {
        if (modProg != file.getMod()) {
            modProg = file.getMod();
        }
        return modProg;
    }

    /**
     * Method that gives back the maximum age, so the maximum number of days a cell can live set by the user.
     * @return maximum age
     */
    public static int getMaxAge() {
        if (maxAge != file.getMaxAge()) {
            maxAge = file.getMaxAge();
        }
        return maxAge;
    }

    /**
     * Method that returns the hue used by color filters set by the user.
     * @return hue
     */
    public static float getHueColor() {
        if (hueColor != file.getHue()) {
            hueColor = file.getHue();
        }
        return hueColor;
    }

    /**
     * Method that returns the maximum energy that can be stored by a single cell set by the user.
     * @return maximum energy
     */
    public static int getMaxEnergy() {
        if (maxEnergy != file.getMaxEnery()) {
            maxEnergy = file.getMaxEnery();
        }
        return maxEnergy;
    }

    /**
     * Method that returns the maximum value of energy that a cell can obtain through photosynthesis set by the user.
     * @return maximum value of energy that a cell can obtain
     */
    public static int getMaxLight() {
        if (maxLight != file.getSunLight()) {
            maxLight = file.getSunLight();
        }
        return maxLight;
    }

    /**
     * Method that returns the number of seconds each time you want to update the graphical user interface set by the user.
     * @return number of seconds each time you want to update the graphical user interface
     */
    public static int getUpDateView() {
        if (upDateView != file.getUpDateView()) {
            upDateView = file.getUpDateView();
        }
        return upDateView;
    }

    /**
     * Method that returns color filter type to display the world set by the user.
     * @return color filter type
     */
    public static Filters getColorFilter() {
        if (colorFilter != file.getColorFilter()) {
            colorFilter = file.getColorFilter();
        }
        return colorFilter;
    }

    /**
     * Method that returns the number of cells present in the height of the world set by the user.
     * @return number of cells in height
     */
    public static int getWorldHeight() {
        if (worldHeight != file.getHeightWorld()) {
            worldHeight = file.getHeightWorld();
        }
        return worldHeight;
    }

    /**
     * Method that returns the number of cells present in the width of the world set by the user.
     * @return number of cells in width
     */
    public static int getWorldWidth() {
        if (worldWidth != file.getWidthWorld()) {
            worldWidth = file.getWidthWorld();
        }
        return worldWidth;
    }

    /**
     * Method that returns the percentage of the lines from the top where the totality of the sun will be present,
     * set by the user.
     * @return percentage of lines whit sun
     */
    public static float getSunPenetration() {
        if (sunPenetration != file.getSunPenetration()) {
            sunPenetration = file.getSunPenetration();
        }
        return sunPenetration;
    }

    /**
     * Method that returns the value of the length of the genome, therefore the number of genes that constitute it,
     *  set by the user.
     * @return length of the genome
     */
    public static int getSizeGenoma() {
        if (sizeGenoma != file.getSizeGenoma()) {
            sizeGenoma = file.getSizeGenoma();
        }
        return sizeGenoma;
    }

    /**
     * Method that returns the percentage of rows from the bottom where the totality of the ore will be present, set by the user.
     * @return percentage of lines whit mineral
     */
    public static float getMineralDepth() {
        if (mineralDepth != file.getMineralDepth()) {
            mineralDepth = file.getMineralDepth();
        }
        return mineralDepth;
    }

}
