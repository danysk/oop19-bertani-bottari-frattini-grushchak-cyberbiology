package controller.file.data.input;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import controller.StartingMode;
import model.properties.defaultdata.ViewDefaultUtils;

/**
 * Class that uploads data passed to a file by argument.
 *
 */
public class FileSetImpl implements FileSet {

    /**
     * Files on which data will be saved.
     * 
     */
    private final File f = new File(System.getProperty("user.dir"), "data.txt"); 

    @Override
    public final void addtoFile(final int maxEnergy, final int maxSunLight, final int sizeGenoma, final int maxAge, final int worldHSize, final int worldWSize,
            final int upDateview, final int filterValue, final StartingMode mod, final float sunPenetration, final float mineralDepth,
            final float hue) {
        try (DataOutputStream dstream = new DataOutputStream(
                new BufferedOutputStream(
                        new FileOutputStream(f)));) {
            dstream.flush();
            dstream.writeInt(maxEnergy);
            dstream.writeInt(maxSunLight);
            dstream.writeInt(sizeGenoma);
            dstream.writeInt(maxAge);
            dstream.writeInt(worldHSize);
            dstream.writeInt(worldWSize);
            dstream.writeInt(upDateview);
            dstream.writeInt(filterValue);
            switch (mod) {
                case RANDOM_GENOME_CELLS: dstream.writeInt(0); break;
                case SINGLE_PHOTOSYNTHESIS_CELL: dstream.writeInt(1); break;
                default: throw new IllegalArgumentException("ENUM NON ESISTENTE");
            }
            dstream.writeFloat(sunPenetration);
            dstream.writeFloat(mineralDepth);
            dstream.writeFloat(hue);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void addtoFile(final int maxEnergy, final int maxSunLight, final int sizeGenoma, final int maxAge, final int worldHSize,
            final int worldWSize, final int upDateview, final int filterValue, final StartingMode mod, final float sunPenetration, final float mineralDepth) {
        addtoFile(maxEnergy, maxSunLight, sizeGenoma, maxAge, worldHSize, worldWSize, upDateview, filterValue, mod,
                sunPenetration, mineralDepth, ViewDefaultUtils.COLOR_HSB_RANGE.getDafaultValue());
    }

    @Override
    public final File getFile() {
        return f;
    }
}
