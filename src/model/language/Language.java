package model.language;

import java.util.ResourceBundle;

/**
 * Class that handles the setting of the chosen bundle and the return of a 
 * string related to the set bundle key.
 *
 */
public class Language implements LanguageSet {

    private static ResourceBundle currentbundle;
    private static Languages currentLanguage;

    @Override
    public final void setcurrentbundle(final Languages l) {
        currentLanguage = l;
        currentbundle = ResourceBundle.getBundle("resource.i18n/Bundle", l.getLocale());
    }

    /**
     * Method that takes a bundle key returns the translated string in the set language.
     * @param key bundle key
     * @return key string
     * @throws IllegalStateException if you have not yet set the language to translate it into
     */
    public static final String getkeyofbundle(final String key) {
        if (currentbundle == null) {
            throw new IllegalStateException("LINGUA NON ANCORA SELEZIONATA");
        }
        return currentbundle.getString(key);
    }

    /**
     * Method that returns the language the program is translated into .
     * @return language
     */
    public static final Languages getCurrentLanguage() {
        if (currentLanguage == null) {
            throw new IllegalStateException();
        }
        return currentLanguage;
    }

}

