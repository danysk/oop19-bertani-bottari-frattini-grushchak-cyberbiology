package model.properties.utilities;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;

/**
 * 
 * Test for {@link NumbersComparator}.
 *
 */
public class TestNumbersComparator {
    /**
     * test.
     */
    @Test
    public void test() {
        final String s = "first number is bigger";
        assertTrue(NumbersComparator.isBiggerThan(0.5F, 0.04999999999999d), s);
        assertTrue(NumbersComparator.isBiggerThan(0.02000000000001d, 0.02d), s);
        assertTrue(NumbersComparator.isBiggerThan(0.1f, -0.9f), s);
        assertTrue(NumbersComparator.isBiggerOrEqulalThan(9999, 9999), s);
        assertTrue(NumbersComparator.isBiggerOrEqulalThan(0.009f, 0.008f), s);
    }
}
