package model.properties.utilities;

import java.math.BigDecimal;

/**
 * 
 * Comparator for {@link Number}s.
 *
 */
public interface NumbersComparator {

    /**
     * Verify if first element is bigger or equal to second argument.
     * @param num1 first number.
     * @param num2 second number.
     * @return true is first number is bigger or equal to second argument.
     */
    static boolean isBiggerOrEqulalThan(final Number num1, final Number num2) {
        return compare(num2, num2) >= 0;
    }

    /**
     * Verify if first element is bigger than second argument.
     * @param num1 first number.
     * @param num2 second number.
     * @return true is first number is bigger than second argument.
     */
    static boolean isBiggerThan(final Number num1, final Number num2) {
       return compare(num1, num2) > 0;
    }

    /**
     * Compare two numbers.
     * @param num1 first number.
     * @param num2 second number.
     * @return 0 if num1 == num2,
     *        -1 if num1 < num2,
     *         1 if num1 > num2.
     */
    static int compare(final Number num1, final Number num2) {
        final BigDecimal bnum1 = new BigDecimal(num1.toString());
        final BigDecimal bnum2 = new BigDecimal(num2.toString());
        return bnum1.compareTo(bnum2);
    }
}
