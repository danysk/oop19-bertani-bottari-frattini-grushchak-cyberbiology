package model.genome.decryptor;

import model.genome.factory.GenesEnum;
import model.genome.genes.Gene;

/**
 * 
 * A gene decryptor offers access to the genes.
 *
 */
public interface GeneDecryptor {
    /**
     * @param index of a gene.
     * @return A gene that correspond to the index.
     * @throws IllegalArgumentException if the index has not corresponded gene.
     */
    Gene getGeneOfIndex(int index);

    /**
     * Controls if index has a correspond gene.
     * @param index of a possible gene.
     * @return true if the index has a corresponded gene.
     */
    boolean isGenePresent(int index);

    /**
     * Return index of gene.
     * @param gene an enum that represent a gene.
     * @return index of gene associated with the enum. If the gene has some indexes then first found will be returned.
     * @throws IllegalArgumentException if the gene has not an index.
     */
    int getIndexOfGene(GenesEnum gene);

    /**
     * @param geneEnum an enum that correspond to an gene.
     * @return a gene that correspond to the input enum.
     */
    Gene getGeneOfEnum(GenesEnum geneEnum);
}
