package model.entity.cell.standard;

import java.util.List;

import model.direction.Direction;
import model.entity.cell.standard.action.ActionsManipulation;

/**
 * the builder to create any sort of cell.
 *
 *
 */
public interface CellStandardBuilder {
    /**
     * 
     * @return
     * the new cell.
     */
    CellStandard build();
    /**
     * 
     * @param x
     * the x 
     * @return
     * the builder
     */
    CellStandardBuilder setX(int x);
    /**
     * 
     * @param y
     * the y
     * @return
     * the builder
     */
    CellStandardBuilder setY(int y);
    /**
     * 
     * @param direction
     * the new direction
     * @return
     * the builder, if direction isnt used, the direction is north
     */
    CellStandardBuilder setDirection(Direction direction);
    /**
     * 
     * @param energy
     * the energy.
     * @return
     * the builder
     */
    CellStandardBuilder setEnergy(int energy);
    /**
     * 
     * @param mineral
     * the mineral.
     * @return
     * the builder
     */
    CellStandardBuilder setMineral(int mineral);
    /**
     * 
     * @param genome
     * a list of integer (no the interface Genome)
     * @return
     * the builder
     */
    CellStandardBuilder setGenome(List<Integer> genome);
    /**
     * 
     * @param actions
     * the action.
     * @return
     * the builder
     */
    CellStandardBuilder setActions(ActionsManipulation actions);
    /**
     *
     * @param generation
     * the generation of the cell (increment +1)
     * @return
     * the builder
     */
    CellStandardBuilder setGeneration(int generation);

}
