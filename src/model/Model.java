package model;

/**
 * interface to start the simulation.
 *
 */
public interface Model {

    /**
     * method that runs cells in the world.
     */
    void start();

    /**
     * 
     * @return number of iterations done so far
     */
    int getIteration();

}
