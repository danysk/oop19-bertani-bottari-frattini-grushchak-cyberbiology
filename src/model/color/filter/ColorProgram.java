package model.color.filter;

import java.awt.Color;

/**
 * Enumeration containing the types of colors of the entities belonging to the world that do not require filters.
 *
 */
public enum ColorProgram {
    /**
     * World background color, LIGHT GREY.
     */
    BACKGROUND_COLOR(225, 225, 225),
    /**
     * Color of dead cells, GREY A LITTLE DARKER THAT THE WORLD.
     */
    CELL_DEATH_COLOR(192, 192, 192),
    /**
     * Color of the rocks, GREY.
     */
    STONE_COLOR(95, 95, 95);

    private Color c;

    ColorProgram(final int r, final int g, final int b) {
        c = new Color(r, g, b);
    }

    /**
     * Method that returns the color of the corresponding enum.
     * @return enum color
     */
    public Color getColor() {
        return c;
    }

}
