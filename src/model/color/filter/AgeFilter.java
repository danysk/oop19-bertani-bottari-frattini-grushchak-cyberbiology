package model.color.filter;

import java.awt.Color;

import controller.file.data.input.DataProgramUtils;
import model.entity.cell.standard.CellStandard;

/**
 * Class that manages the color of standard cells according to their age. Starting with the default or user-set tone and 
 * changing the brightness according to age. 
 * 
 */
public class AgeFilter extends FilterImpl {

    private static final int MAX_AGE = DataProgramUtils.getMaxAge();
    private static final float HUE = DataProgramUtils.getHueColor();

    /**
     * Calculates the color of the passed STANDARD_CELL as a parameter based on its age.
     * Starting from a hue chosen by the user, its brightness decreases as the cell ages.
     * LIGHT COLORS -> YOUNG CELLS
     * DARK COLORS -> OLD CELLS
     * @param cellstandard standard cell of which you want to know the color
     * @return cell color 
     */
    @Override
    protected final Color getCellStandardColor(final CellStandard cellstandard) {
        return Color.getHSBColor(HUE, 1, brightness(cellstandard.getAge()));
    }

    private float brightness(final int age) {
        return (float) (MAX_AGE - age) / MAX_AGE;
    }

}
