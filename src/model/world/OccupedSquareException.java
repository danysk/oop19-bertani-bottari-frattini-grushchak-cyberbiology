package model.world;

/**
 * exception launched when you try to put a cell in an occuped square.
 *
 */
public class OccupedSquareException extends RuntimeException {

    /**
     * automatically generated.
     */
    private static final long serialVersionUID = 1L;

    /**
     * construct a new OccupedSquareException with a message error.
     * @param message 
     *                  the error message
     */
    public OccupedSquareException(final String message) {
        super(message);
    }
}
