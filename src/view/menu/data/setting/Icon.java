package view.menu.data.setting;

import java.net.URL;
import javax.swing.ImageIcon;

/**
 * Enumeration of the icon used by the application.
 */
public enum Icon {

    /**
     * Enum of the icon for this application menu window.
     */
    APPLICATION("image/genomy.icon.png"),
    /**
     * Enum of the icon for this color menu window.
     */
    COLOR("image/color.icon.png"),
    /**
     * Enum relativa all'icona info del programma.
     */
    INFO("image/info.icon.png"),
    /**
     * Enum relativa all'icona logo del programma.
     */
    LOGO("image/genomy.logo.png"),
    /**
     * Enum of the icon for this language menu window.
     */
    LANGUAGE("image/language.icon.png"),
    /**
     * Enum of the icon for this setting menu window.
     */
    SETTING("image/settings.icon.png");

    private ImageIcon icon;

    /**
     * Constructor that initializes an enum by assigning it its icon from the string that identifies its path.
     * @param name the string that identifies path
     */
    Icon(final String name) { 
        final URL iconURL = ClassLoader.getSystemResource(name);
        this.icon = new ImageIcon(iconURL);
    }

    /**
     * Method that returns the corresponding enum icon.
     * @return icon
     */
    public ImageIcon getIcon() {
        return icon;
    }

}
