package view.menu.data.setting;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Class that manages a panel to set the values between two extremes passed to the manufacturer starting from a default value.
 *
 */
public class AddSlider extends JPanel implements AddElemValue<Integer> {

    private static final long serialVersionUID = -3677584699835834509L;
    private JSlider slider;
    private int value;
    private final JLabel val = new JLabel();

    /**
     * Constructor that implements a panel with a title passed to the constructor and a slicer between a range 
     * passed per value and a default value passed to the constructor.
     * @param string title
     * @param min minimum range value
     * @param max maximum range value
     * @param valDefault default value
     */
    public AddSlider(final String string, final int min, final int max, final int valDefault) {
        super();
        this.value = valDefault;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(text(string));
        this.add(setSlicer(min, max));
        this.add(new JSeparator());
    }

    /**
     * Constructor that implements a panel with a title passed to the constructor and a slicer between a range 
     * passed per value and a default value passed to the constructor, multiplied by 100.
     * @param string title
     * @param min minimum range value
     * @param max maximum range value
     * @param valDefault default value
     */
    public AddSlider(final String string, final float min, final float max, final float valDefault) {
        this(string, (int) (min * 100), (int) (max * 100), (int) (valDefault * 100));
    }

    @Override
    public final Integer getValue() {
        return value;
    }

    @Override
    public final JPanel getElem() {
        return this;
    }

    private JPanel text(final String string) {
        final JPanel panel = new JPanel();
        panel.add(new JLabel(string));
        panel.add(val);
        return panel;
    }

    private JSlider setSlicer(final int min, final int max) {
        slider = new JSlider(min, max);
        slider.setValue(value);
        slider.setPaintTrack(true); 
        slider.setPaintTicks(true);
        slider.setMajorTickSpacing(max / 2); 
        slider.setMinorTickSpacing(max / 4);
        val.setText(" " + slider.getValue());


        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(final ChangeEvent e) {
                val.setText(" " + slider.getValue());
                value = slider.getValue();
            }
        });
        return slider;
    }
}
