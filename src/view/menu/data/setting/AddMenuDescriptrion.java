package view.menu.data.setting;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import model.language.Language;

/**
 * Class that implements a panel containing the description of the menu to set the parameters.
 *
 */
public class AddMenuDescriptrion extends JPanel implements AddElem {

    private static final long serialVersionUID = -3347005479021142881L;
    private final JLabel description = new JLabel(Language.getkeyofbundle("MenuDescription"));

    /**
     * Constructor that implements a panel containing separators and a string with the description of the menu.
     */
    public AddMenuDescriptrion() {
        super();
        this.setPreferredSize(DimensionComponent.MENU_DESCRIPTION_PANEL.getDimension());
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(new JSeparator());
        description.setAlignmentX(CENTER_ALIGNMENT);
        this.add(description);
        this.add(new JSeparator());
        this.add(new JSeparator());
    }

    @Override
    public final JPanel getElem() {
        return this;
    }
}
