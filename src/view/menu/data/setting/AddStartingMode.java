package view.menu.data.setting;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import controller.StartingMode;

/**
 * Class that manages a panel to set how the world will be initialized.
 *
 */
public class AddStartingMode extends JPanel implements AddElemValue<StartingMode> {

    /**
     */
    private static final long serialVersionUID = -411327285799347463L;
    private final List<JButton> buttons = new LinkedList<>();
    private StartingMode modSelect;

    /**
     * Constructor that implements a panel containing as many keys as there are modes that you can set.
     * @param desc panel description
     */
    public AddStartingMode(final String desc) {
        super();
        final JLabel jl = new JLabel(desc);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        jl.setAlignmentX(CENTER_ALIGNMENT);
        this.add(jl);
        this.add(addButtons());
        this.add(new JSeparator());
    }

    private JPanel addButtons() {
        final JPanel panel = new JPanel();
        Arrays.asList(StartingMode.values()).forEach(mod -> {
            final JButton jb = new JButton(mod.toString());
            if (mod == StartingMode.SINGLE_PHOTOSYNTHESIS_CELL) {
                jb.setEnabled(false);
                modSelect = StartingMode.SINGLE_PHOTOSYNTHESIS_CELL;
            }
            jb.addActionListener(a -> {
                final JButton jbSelect = (JButton) a.getSource();
                buttons.forEach(bt -> bt.setEnabled(true));
                jbSelect.setEnabled(false);
                modSelect = StartingMode.valueOf(jbSelect.getText());
            });
            panel.add(jb);
            buttons.add(jb);
        });
        return panel;
    }

    @Override
    public final AddStartingMode getElem() {
        return this;
    }

    @Override
    public final StartingMode getValue() {
        return modSelect;
    }

}
