package view.menu.data.setting;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.color.filter.Filters;
import model.language.Language;

/**
 * Class that manages a panel containing a ComboBox in which are set the types of color filters that can be set in the 
 * program, also has a button that according to the filter chosen can allow you to set a tone for the filter.
 */
public class AddFilterColor extends JPanel implements AddElemValue<Integer> {

    private static final long serialVersionUID = 2751971486480470019L;
    private int value = Filters.AGE.getValue();
    private final JButton colorChoose = new JButton();
    private final AddElemValue<Float> frameColorChoose;
    private final JLabel description = new JLabel(Language.getkeyofbundle("Colorfilter"));

    /**
     * Constructor that implements a panel containing a ComboBox with the types of color filters, and a button that if pressed 
     * allows you to choose the tone of the filter by opening a new window.
     * @param frameColorChoose object containing the window for choosing the color
     */
    public AddFilterColor(final AddElemValue<Float> frameColorChoose) {
        super();
        this.frameColorChoose = frameColorChoose;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        description.setAlignmentX(CENTER_ALIGNMENT);
        this.add(description);
        this.add(colorfilter());
        this.add(buttonColor());
    }

    @Override
    public final JPanel getElem() {
        return this;
    }

    @Override
    public final Integer getValue() {
        return value;
    }

    private JPanel colorfilter() {
        final JPanel panel = new JPanel();
        final JComboBox<String> combo = new JComboBox<>();
        Arrays.asList(Filters.values()).forEach(filter -> combo.addItem(Language.getkeyofbundle(filter.getName())));
        panel.add(combo);
        combo.addActionListener(new ActionListener() {
                public void actionPerformed(final ActionEvent event) {
                value = combo.getSelectedIndex();
                enableColorChoose();
            }
        });
        return panel;
    }

    private JPanel buttonColor() {
        final JPanel panel = new JPanel();
        final JLabel text = new JLabel(Language.getkeyofbundle("Colorchoose"));
        text.setAlignmentX(CENTER_ALIGNMENT);
        panel.add(text);
        colorChoose.setText(Language.getkeyofbundle("Click"));
        colorChoose.setAlignmentX(CENTER_ALIGNMENT);
        colorChoose.addActionListener(l -> frameColorChoose.getElem());
        enableColorChoose();
        panel.add(colorChoose);
        return panel;
    }

    private void enableColorChoose() {
        if (value == Filters.NUTRITION.getValue()) {
            colorChoose.setEnabled(false);
            colorChoose.setBorder(BorderFactory.createLoweredBevelBorder());
            colorChoose.setBackground(Color.LIGHT_GRAY);
        } else {
            colorChoose.setEnabled(true);
            colorChoose.setBorder(BorderFactory.createRaisedBevelBorder());
            colorChoose.setBackground(null);
        }
    }


}
